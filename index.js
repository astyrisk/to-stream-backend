const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3001;

const PIPEDINSTANCE = 'https://pipedapi.kavin.rocks';
const INVIDIOUSINSTANCE = 'https://invidious.projectsegfau.lt';

app.use(cors());

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/search', (req, res) => {
    let filterInput = "all";

    const searchQuery = '/search?q=' + req.query.search;
    const filterQuery = '&filter=' + filterInput;

    console.log(PIPEDINSTANCE + searchQuery + filterQuery)
    fetch (PIPEDINSTANCE + searchQuery + filterQuery)
        .then(results => results.json())
        .then(async (results) => {
            // filters shorts and live videos
          // console.log(results.items);
            let items = results.items.filter((item) => !item.isShort && item.duration !== -1);
            res.send(items);
        });
    // res.send(dataToSend);
});
 
// quite obselete, but ready to get scaled
app.get('/audio/:id', async (req, res) => {
  const id = req.params.id;
  const data = await fetch(INVIDIOUSINSTANCE + '/api/v1/videos/' + id + '?fields=title,lengthSeconds,adaptiveFormats,author,authorUrl,authorThumbnails,recommendedVideos')
      .then(res => res.json());
  if (!data?.adaptiveFormats?.length)
    return;

  const audioStreams = data.adaptiveFormats
    .filter((_) => _.hasOwnProperty('audioChannels'))
    .sort((a, b) => (a.bitrate - b.bitrate));

    const url = (audioStreams[0].url).replace(new URL(audioStreams[0].url).origin, INVIDIOUSINSTANCE);
  res.send(url);


  // audioStreams.forEach((_, i) => {
  //   const bitrate = parseInt(_.bitrate);
  //   const codec = _.type.includes('opus') ? 'opus' : 'aac';
  //   const quality = Math.floor(bitrate / 1024) + ' kbps ' + codec;

  //   const url = (_.url).replace(new URL(_.url).origin, INVIDIOUSINSTANCE);
  //   res.send(url);
  //   });
});

app.get('/stream/:id', (req, res) => {
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
