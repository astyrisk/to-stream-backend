/* consts */

//TODO support lazy loading for next page. write setObserver function
const PipedInstance = 'https://pipedapi.kavin.rocks';
// const PipedInstance = 'https://piped-api.lunar.icu/';

let searchInput = "";
let filterInput = "all";

const args = process.argv.slice(2);
searchInput = args[0];

const searchQuery = '?q=' + searchInput;
const filterQuery = '&filter=' + filterInput;
const query = 'search' + searchQuery + filterQuery;

function loadMoreResults(query, token) {
  fetch(`${PipedInstance}/nextpage/search?nextpage=${encodeURIComponent(token)}&${query}`)
    .then(res => res.json())
    .then(x => console.log(x));
}

console.log(PipedInstance + '/' + query)

function searchLoader() {
  fetch(PipedInstance + '/' + query)
    .then(
      res => res.json()
    )
    .then(async (res) => {
      let items = res.items.filter((item) => !item.isShort && item.duration !== -1);
      // items = items.filter((item) => !item.isShort && item.duration !== -1)
      console.log(items);
      // let nextPageToken = res.nextpage;
      // if (!items) {
      //   throw new Error('Search could\'nt be resolved on' + pipedInstances);
      // }
    });
}

searchLoader()
