// const INV = 'https://invidious.fdn.fr';
const INV = 'https://invidious.projectsegfau.lt';
let id = '3r-qDvD3F3c';

async function fetchVideoData() {
 try {
    const data = await fetch(INV + '/api/v1/videos/' + id + '?fields=title,lengthSeconds,adaptiveFormats,author,authorUrl,authorThumbnails,recommendedVideos')
      .then(res => res.json());
    // console.log(data);
  if (!data?.adaptiveFormats?.length)
    return;

  const audioStreams = data.adaptiveFormats
    .filter((_) => _.hasOwnProperty('audioChannels'))
    .sort((a, b) => (a.bitrate - b.bitrate));


   console.log(audioStreams);

  audioStreams.forEach((_, i) => {
    const bitrate = parseInt(_.bitrate);
    const codec = _.type.includes('opus') ? 'opus' : 'aac';
    const quality = Math.floor(bitrate / 1024) + ' kbps ' + codec;

    const url = (_.url).replace(new URL(_.url).origin, INV);
    console.log(url);

    // proxy the url
    // add to DOM
    // bitrateSelector.add(new Option(quality, url));

    });
   // console.log(audioStreams)
 //  const noOfBitrates = audioStreams.length;

 //  if (!noOfBitrates) {
 //    console.log("no audio streams available!");
 //    return;
 //  }

 } catch (error) {
    console.error('Error fetching video data:', error);
 }


}

fetchVideoData();
